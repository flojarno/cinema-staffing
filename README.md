# Java - Gestion du Personnel d'un Cinéma

Ce projet est une introduction aux bases du langage Java à travers un programme simple de gestion du personnel pour un cinéma. Le programme permet de stocker des informations sur chaque employé et de calculer leur salaire en fonction des heures travaillées et de leur taux horaire.

## Fonctionnalités

- **Calcul du Salaire Hebdomadaire** : Calcul du salaire en fonction des heures normales et des heures supplémentaires (taux 1.5x après 35 heures).
- **Recherche par Poste** : Possibilité de lister les employés selon leur poste spécifique avec gestion des cas où aucun employé n'est trouvé pour le poste recherché.

## Démarrage Rapide

1. **Clonez le dépôt** :
   ```bash
   git clone git@gitlab.com:flojarno/cinema-staffing.git
   ```
2. Ouvrez le projet dans IntelliJ IDEA ou tout autre IDE Java supportant les projets standard Java.

3. Compilez et exécutez le programme
- Naviguez dans le dossier racine du projet.
- Compilez le fichier source :
   ```bash
  javac Main.java
   ```
- Exécutez le programme compilé :
     ```bash
  java Main
   ```