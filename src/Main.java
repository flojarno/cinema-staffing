import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {

        String[] employeeNames = { "Alice Dupont", "Bob Martin", "Charlie Besson", "Diane Loriot", "Eva Joly" };
        int[] hoursWorked = { 35, 38, 35, 38, 40 };
        double[] hourlyRates = { 12.5, 15.0, 13.5, 14.5, 13.0 };
        String[] positions = { "Caissier", "Projectionniste", "Caissier", "Manager", "Caissier" };

        for (int i = 0; i < employeeNames.length; i++) {
            double salary = 0;
            int extraHours = 0;
            if (hoursWorked[i] > 35) {
                extraHours = hoursWorked[i] - 35;
                salary = 35 * hourlyRates[i] + extraHours * hourlyRates[i] * 1.5;
            } else {
                salary = hoursWorked[i] * hourlyRates[i];
            }
            System.out.println(employeeNames[i] + " = " + salary );
        }

        String searchPosition = "Caissier";
        ArrayList<String> positionFound = new ArrayList<>();

        for (int i = 0; i < employeeNames.length; i++) {
            if (positions[i].equals(searchPosition)){
                System.out.println(employeeNames[i]);
                positionFound.add(employeeNames[i]);
            }
        }

        if (positionFound.isEmpty()){
               System.out.println("Aucun employé trouvé.");
        }
    }
};